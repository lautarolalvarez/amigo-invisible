'use strict';
const nodemailer = require('nodemailer');
require('dotenv').config();

// SMTP configuration
const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURE === 'true',
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS
    }
});

// Array of people
let people = [{
  name: 'Lionel Messi',
  mail: 'elmessi10@mail.com',
  assigned: '',
  sended: false,
  tries: 0
}, {
  name: 'Luca Modric',
  mail: 'modric_luca@mail.com',
  assigned: '',
  sended: false,
  tries: 0
}];

// Mail info
function sendMailTo(person) {
  let mailOptions = {
    from: `"Amigo Invisible" <${process.env.SMTP_USER}>`,
    to: person.mail,
    subject: 'Amigo Invisible',
    html: `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <title></title>
          <style>
            h2 {
               font-weight: normal;
            }
          </style>
        </head>
        <body>
          <h2>Hola ${person.name},</h2>
          <h2>Te toca comprarle un regalo a <b>${person.assigned}</b></h2>
        </body>
      </html>
      </html>
    `
  };
  return transporter.sendMail(mailOptions)
    .catch((error) => {
      if (person.tries > 4) {
        console.log('----Error al enviar mail a: ', person.name);
        console.log('Msg: ', error.message);
        console.log('----');
        return null;
      }
      person.tries++;
      return sendMailTo(person);
    });
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function assignOther() {
  let peopleNames = people.map(person => {
    return person.name;
  });
  shuffleArray(peopleNames);
  people = people.map(person => {
    person.assigned = peopleNames.pop();
    return person;
  });

  let repeated = people.find(elem => {
    return elem.name == elem.assigned;
  });
  if (repeated) {
    return assignOther();
  }
}

function saveCopy() {
  return new Promise((resolve, reject) => {
    var fs = require('fs');
    let bkp = '';
    people.forEach(person => {
      bkp += person.name + ' -> ' + person.assigned + '\n';
    });
    fs.writeFile("./resultado.bkp", bkp, function(err) {
        if (err) {
            return reject(err);
        }
        return resolve();
    });
  });
}

function sendMailsToPendings() {
  return Promise.all(people.map(person => {
    return sendMailTo(person);
  }))
}

console.log('Iniciando');
console.log('1. Asignando personas');
assignOther();
console.log('2. Realizando backup de las asignaciones (resultado.bkp)');
return saveCopy()
  .then(() => {
    console.log('3. Enviando mails');
    return sendMailsToPendings();
  })
  .then(() => {
    console.log('Finalizado con exito!');
  })
  .catch((error) => {
    console.error('Hubo un error: ', error.message);
  });
